<?php

namespace Drupal\jsonapi_focal_point\Plugin\jsonapi\FieldEnhancer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\focal_point\FocalPointManagerInterface;
use Drupal\jsonapi_extras\Plugin\ResourceFieldEnhancerBase;
use Shaper\Util\Context;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add URL aliases to links.
 *
 * @ResourceFieldEnhancer(
 *   id = "focal_point",
 *   label = @Translation("Focal Point (image field only)"),
 *   description = @Translation("Add Focal Point data.")
 * )
 */
class FocalPointEnhancer extends ResourceFieldEnhancerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Focal Point manager.
   *
   * @var \Drupal\focal_point\FocalPointManagerInterface
   */
  protected $focalPointManager;

  /**
   * Constructs FocalPointEnhancer.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\focal_point\FocalPointManagerInterface $focal_point_manager
   *   The Focal Point manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    FocalPointManagerInterface $focal_point_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->focalPointManager = $focal_point_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('focal_point.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doUndoTransform($data, Context $context) {
    // Check if File ID is present.
    if (isset($data['meta']['drupal_internal__target_id'])) {
      $file_id = $data['meta']['drupal_internal__target_id'];

      $image_width = $data['meta']['width'] ?? 0;
      $image_height = $data['meta']['height'] ?? 0;

      $file = $this->entityTypeManager->getStorage('file')->load($file_id);
      if (isset($file)) {
        $crop = $this->focalPointManager->getCropEntity($file, 'focal_point');
        $position = $crop->position();

        if ($position) {
          $data['meta']['focal_point'] = [
            'x' => $position['x'],
            'y' => $position['y'],
          ];

          if ($image_width > 0 && $image_height > 0) {
            $data['meta']['focal_point']['x_percent'] = ($position['x'] / $image_width) * 100;
            $data['meta']['focal_point']['y_percent'] = ($position['y'] / $image_height) * 100;

          }
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransform($data, Context $context) {
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputJsonSchema() {
    return [
      'type' => 'object',
    ];
  }

}
